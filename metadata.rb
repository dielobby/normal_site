name 'normal_site'
maintainer 'Paul Ilea'
maintainer_email 'p.ilea@die-lobby.de'
license 'Apache 2.0'
description 'Site'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version '1.0.0'
recipe 'normal_site::default', 'Main recipe'
issues_url 'https://bitbucket.org/dielobby/normal_site/issues'
source_url 'https://bitbucket.org/dielobby/normal_site'

depends 'apache2', '<= 3.2.1'
depends 'database', '~> 5.1.2'
depends 'graphicsmagick'
depends 'hostsfile'
depends 'java', '<= 2.2.1'
depends 'line', '<= 2.1.1'
depends 'mysql', '~> 7.2.0'
depends 'mysql2_chef_gem', '~> 1.1.0'
depends 'composer', '~> 2.6.1'
depends 'nodejs', '~> 6.0.0'
depends 'seven_zip', '< 3.0.0'
depends 'ssh_known_hosts', '< 7.0.0'
depends 'yum-mysql-community', '<= 4.0.1'
depends 'apt', '<= 7.1.1'
depends 'ark', '<= 4.0.0'
depends 'mariadb', '<= 1.5.4'
depends 'selinux_policy', '<= 2.1.0'
depends 'ubuntu_base'
