default['normal_site']['ip_address'] = false
default['normal_site']['hostname'] = false
default['normal_site']['webroot'] = '/var/www'
default['normal_site']['server_aliases'] = 'www.site.dev'
default['normal_site']['web_directory'] = ''
default['normal_site']['allow_http2'] = false
default['normal_site']['install_ioncube'] = false

if node['platform_version'] == '16.04'
	default['normal_site']['mysql_version'] = '5.7'
else
	default['normal_site']['mysql_version'] = '5.6'
end

# Example:
# [
#   {
#       :ip_address => '127.0.0.1',
#       :hostname => 'project.dev',
#   }
# ]
default['normal_site']['create_additional_hostentries'] = []

# Example:
# [
#   {
#       :folder => '/var/www/newFolder/',
#   }
# ]
default['normal_site']['create_additional_folders'] = []


# Example:
#[
#	{
#		:repository => 'git@bitbucket.org:namespace/project.git',
#		:folder_name => 'someFolder',
#		:hostname => 'bitbucket.org',
#		:port => 22,
#	},
#	{
#		:repository => 'git@bitbucket.org:namespace/project2.git',
#		:folder_name => 'someFolder',
#		:hostname => 'bitbucket.org',
#		:port => 22,
#	}
#]
default['normal_site']['sync_repositories'] = []

# Example:
#[
#	{
#		:user => 'vagrant',
#		:hostname => 'deploy.site.com',
#		:port => 22,
#		:remote => '~/site/fileadmin/',
#		:local => 'fileadmin',
#		:sync => true
#	},
#	{
#		:user => 'vagrant',
#		:hostname => 'deploy.site.com',
#		:port => 22,
#		:remote => '~/site/uploads/',
#		:local => 'uploads',
#		:sync => true
#	}
#]
default['normal_site']['sync_directories'] = []

# Example:
#[
#	{
#		:database_name => 'typo3',
#		:database_user => 'typo3',
#		:database_password => 'typo3',
#		:dump_user => 'vagrant',
#		:dump_hostname => 'deploy.site.com',
#		:dump_port => 22,
#		:dump_remote => '~/site/dump.sql',
#		:dump_local => '/home/vagrant/dump.sql',
#		:post_install_queries => [
#			"INSERT INTO typo3.be_users (pid, tstamp, username, password, admin, usergroup, disable, starttime, endtime, lang, email) VALUES (0,1276860841,'admin','$1$lV7klIfP$E8.y9jiK4RL6qZzgWboSP/',1,'0',0,0,0,'','admin@example.com')",
#			"UPDATE typo3.sys_domain SET domainName = 'site.dev' WHERE uid = 1;"
#		]
#	}
#]
default['normal_site']['sync_databases'] = []

# Example:
# [
#   {
#       :source => '/var/www/mySite/fileadmin',
#       :target => '/var/www/fileadmin',
#   }
# ]
default['normal_site']['create_links'] = []
